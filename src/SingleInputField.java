import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SingleInputField {

	WebDriver driver;
	final String URL = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";
	final String DRIVER_PATH = "/User/aryasasok/Desktop/chromedriver";
	@Before
	public void setUp() throws Exception {// this webDriver is local so we make it public
		
		//setup selenium +chrome

				System.setProperty("webdriver.chrome.driver","/Users/aryasasok/Desktop/chromedriver");
				WebDriver driver = new ChromeDriver();

		//tell selenium what page to test

				driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");

	}

	@After
	public void tearDown() throws Exception {
		
		Thread.sleep(2000);
		driver.close();
	}


	@Test
	public void testsingleInput() throws Exception{
		
		//selenium+chrome

		System.setProperty("webdriver.chrome.driver","/Users/aryasasok/Desktop/chromedriver");
		WebDriver driver = new ChromeDriver();
		
		
		//selenium + page
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");

		
		WebElement textBox = driver.findElement(By.id("user-message"));
		textBox.sendKeys("Hello world");
		
		
		WebElement button =  driver.findElement(By.cssSelector("form#get-input button" ));
		button.click();
		
		WebElement outputSpan = driver.findElement(By.id("display"));
		String  outputMessage = outputSpan.getText();
		
		assertEquals("Helloworld",outputMessage);
		
		Thread.sleep(2000);
		driver.close();
		

	}
	@Test
	public void testdoubleInput() throws Exception {
		
		//selenium+chrome

				System.setProperty("webdriver.chrome.driver","/Users/aryasasok/Desktop/chromedriver");
				WebDriver driver = new ChromeDriver();
				
				
				//selenium + page
				driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
				
				
				WebElement textbox = driver.findElement(By.id("sum1"));
				textbox.sendKeys("10");
				
				WebElement textbox1 = driver.findElement(By.id("sum2"));
				textbox1.sendKeys("10");
				
				WebElement button = driver.findElement(By.cssSelector("form#form-froup "));
				button.click();
				
				

				WebElement outputSpan = driver.findElement(By.id("displayvalue"));
				String outputMessage = outputSpan.getText();
				
				assertEquals("20", outputMessage);
				
				Thread.sleep(2000);
				driver.close();
				
				
				
	}
	

}
