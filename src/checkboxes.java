import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import java.util.*;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class checkboxes {

	final String URL = "https://www.seleniumeasy.com/test/basic-checkbox-demo.html";
 	final String DRIVER_PATH = "/Users/aryasasok/Desktop/chromedriver";
 	
 	WebDriver driver;
 	
 	@Before
 	public void setUp() throws Exception {
 		// Setup Selenium + Chrome
 		System.setProperty("webdriver.chrome.driver",DRIVER_PATH);
 		driver = new ChromeDriver();
 		
 		// Tell Selenium what page to test
 		driver.get(URL);
 	}
 
 	@After
 	public void tearDown() throws Exception {
 		// At end of test case, wait for a few seconds, then close the browser
 		Thread.sleep(1000);				// OPTION 1
 		//TimeUnit.SECONDS.sleep(1);		// OPTION 2
 		driver.close();
 	}
	@Test
	public void testSinglecheckboxDemo() {
		
/*1.find the checkbox (id=isAgeSelelctor)
 *
2.click on the checkbox
3.find the output message(id=i"txtAge"

)
4.check the output message*/
		
		//selenium+chrome

				System.setProperty("webdriver.chrome.driver","/Users/aryasasok/Desktop/chromedriver");
				WebDriver driver = new ChromeDriver();
				
				
				//selenium + page
				driver.get("https://www.seleniumeasy.com/test/basic-checkbox-demo.html");
				
				WebElement checkbox = driver.findElement(By.id("isAgeSelected"));
				checkbox.click();
				
				//test that checkbox actually got checked
				
				boolean isChecked= checkbox.isSelected();
				assertTrue(isChecked);
				
				WebElement outputDiv = driver.findElement(By.id("txtAge"));
				String actualOuput = outputDiv.getText();
				
				String expectedOutput = "Sucess -checkbox is checked";
				assertEquals(expectedOutput, actualOuput);
	}
	
	@Test
	public void testmulipleVheckboxDemo() {
		
		/*1.Press Button
		->all boxes get checked
		word on the button changes to "uncjeck all"
		
		unselect a chckbox
		*the checkbox is not selected
		*button go to "check all"
		*
		*pressButton
		*1.find the button
		
		*2.click on the button
		*3.check that all boxes are checked
		*4.check the word on button changes to "Uncheck all"
		*
		*Uncheck one checkbox
		*1/find a checkbox
		*2.check that box is checked
		*3.click on box
		*4.chck that box is unchecked
		*5.check that word changes to "check all"
		*
		*/
		
		WebElement button = driver.findElement(By.id("check1"));
		button.click();
		//*3.check that all boxes are checked(all are under the same classs- findEmelents//
		
		driver.findElements(By.className("cb1-element"));
		List<WebElement> checkboxes = driver.findElements(By.className("cb1-element"));
		
		
		//
		
		for(int i=0;i<checkboxes.size();i++)
		{
			WebElement checkbox =checkboxes.get(i);
			boolean isChecked = checkbox.isSelected();
					assertTrue(isChecked);
			
		}
		
	//*4.check the word on button changes to "Uncheck all"//
		String buttonText= button.getText();
		assertEquals("Uncheck All", buttonText);
		
		
		//<input type="button" class="btn btn-primary" id="check1" value="Check All">//
		
		
		WebElement checkboxes2 = checkboxes.get(2);
		checkboxes2.click();
		
		boolean isBoxChecked = checkboxes2.isSelected();
		assertTrue(isBoxChecked);
		
		//*5.check that word changes to "check all"//
		
		
		buttonText = button.getAttribute("Value");
		String expectedOutput = "check All";
		
		assertEquals("Check All",buttonText);
		
		
		
	}
	
	
}
