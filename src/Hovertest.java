import static org.junit.Assert.*;

import java.awt.List;

import org.junit.After;
import org.junit.Before;
import java.util.*;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Hovertest {


	WebDriver driver;
	final String URL = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";
	final String DRIVER_PATH = "/User/aryasasok/Desktop/chromedriver";
	@Before
	public void setUp() throws Exception {// this webDriver is local so we make it public
		
		//setup selenium +chrome

				System.setProperty("webdriver.chrome.driver","/Users/aryasasok/Desktop/chromedriver");
				WebDriver driver = new ChromeDriver();

		//tell selenium what page to test

				driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");

	}

	@After
	public void tearDown() throws Exception {
		
		Thread.sleep(2000);
		driver.close();
	}

	
	@Test
	public void testHover() throws Exception {
/*1.1 Find the image (since all images are same ---right click in image html, copy - copy selector)
 * #content
 *  > div 
 *  > div:nth-child(3) 
 *  > img
 * OR 
 * do as check box way>
 * 
2.hove on he image
3.clcik on the view profile link
4.wait for the system to go to the next page 
5. get the url
6. gte and chck the url are equal*/
		
		
	List<WebElement> images = driver.findElement(By.cssSelector("div.figue img"));
		assertEquals(3,images.size());
		
		WebElement image =images.get(0);
		
		
Actions builder = new Actions(driver);
builder.moveToElement(image).build().perform();

//3.clcik on the view profile link//
List<WebElement> profileLinks = driver.findElement(By.cssSelector("div.figcaptio a");
assertEquals(3,profileLinks.size());

WebElement firstImagelink = profileLinks.get(0);
Thread.sleep(3000);
firstImagelink.click();


String urlPage2 = driver.getCurrentUrl();
System.out.println(urlPage2);


assertEquals("http://the-internet.herokuapp.com/hovers",urlPage2);






	
	
	}

}
