import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class InputboxTest {

	WebDriver driver;
	final String URL = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";
	final String DRIVER_PATH = "/User/aryasasok/Desktop/chromedriver";
	@Before
	public void setUp() throws Exception {// this webDriver is local so we make it public
		
		//setup selenium +chrome

				System.setProperty("webdriver.chrome.driver","/Users/aryasasok/Desktop/chromedriver");
				WebDriver driver = new ChromeDriver();

		//tell selenium what page to test

				driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");

	}

	@After
	public void tearDown() throws Exception {
		
		Thread.sleep(2000);
		driver.close();
	}

	@Test
	public void testSingleInputField() throws Exception {
		
		//setup selenium +chrome

		System.setProperty("webdriver.chrome.driver","/Users/aryasasok/Desktop/chromedriver");
		WebDriver driver = new ChromeDriver();

//tell selenium what page to test

		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");




/*1. Find the textbox
2.type "hello world: into the textbox" (id = "user-message")(.sendKeys("..."))
		+ "3.find the button"  (cssSeslectors =  form#get-input button)
		+ "4.click on the button"
		+ "5.find the output message thing" (id = display)
		+ "6.check the output message(expectedResult = "Hello world"
		
		
		
		
		*/

WebElement textBox = driver.findElement(By.id("user-messgae"));
textBox.sendKeys("Hello World");

WebElement button = driver.findElement(By.cssSelector("form#get-input button"));
button.click();

WebElement outputSpan = driver.findElement(By.id("display"));
String outputMessage = outputSpan.getText();

assertEquals("HELLO WORLD!", outputMessage);
//close the browser
//TimeUnit.SECONDS.sleep(1);  /either this or thread.sleep/
Thread.sleep(2000);
driver.close();
	}

	@Test
	public void testTwoInputField() throws Exception {
		
		
		/*1. Find the textbox (id=sum1)
		 * 2. type "50" into the textbox (.sendkeys(".....")
		 * 3. find the textbox2 (id = sum2)
		 * type  "70" i
		2.type "hello world: into the textbox" (id = "user-message")(.sendKeys("..."))
				+ "3.find the button"  (cssSeslectors =  form#get-input button)
				+ "4.click on the button"
				+ "5.find the output message thing" (id = display)
				+ "6.check the output message(expectedResult = "Hello world"
				
				
				
				
				*/
		
		//setup selenium +chrome

				System.setProperty("webdriver.chrome.driver","/Users/macstudent/Desktop/chromedriver");
				WebDriver driver = new ChromeDriver();

		//tell selenium what page to test

				driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
				
				//write your test case code
				//-----------
				
				
				WebElement textBox = driver.findElement(By.id("sum1"));
				textBox.sendKeys("50");

				WebElement textBox1 = driver.findElement(By.id("sum2"));
				textBox1.sendKeys("70");
				
				WebElement button1 = driver.findElement(By.cssSelector("form#gettotal button"));
				button1.click();

				WebElement outputSpan = driver.findElement(By.id("displayvalue"));
				String outputMessage = outputSpan.getText();

				
				assertEquals("120",outputMessage);
				
				
				//-------------

				
				Thread.sleep(2000);
				driver.close();

	}
}
